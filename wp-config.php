<?php

/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'naturedia.fr' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6Ep:t&I%%cu;+<herOQB*>bF3Tlnb]7}k?eP#7P *?U;;Z4*K)i;H%<8x8f.(7R]' );
define( 'SECURE_AUTH_KEY',  'qj^i,U}H;ssT[mP3i^F8]<Oet~8X>M!n+kMQ^b.aAg<FH,aZe6w-C<&wg_rN|S6r' );
define( 'LOGGED_IN_KEY',    'maJ7ndr1FK968OCLg1Wzm[mks1Tnm;k^,s[%dg$ugC4V=W!7f^jO1~M=lz_zrQCM' );
define( 'NONCE_KEY',        '@AD0DhU<%/bNE}gi`yPb!*P=0w+_]nmO{D~o/z,?LaiciT$PV,KQTgIp2S0Yrvm#' );
define( 'AUTH_SALT',        ':YW&ku|IFL,BF*-bNjsoypYE7{q+W_F@6_ m}:[4?%/0>]>/ ,NRp>f kMAJ;qek' );
define( 'SECURE_AUTH_SALT', 'me9X)]u5@#sMOH6IlQCgM*zm+-3_`]JSG)SD`OJ0YpccGL=d,LAOQjL,vFi/3,l)' );
define( 'LOGGED_IN_SALT',   'Qq({:fmT{_&Ke6,4L&X~(=7&3>PgWtfEd_ZK?|54Z;8|?6GC~loW#p1&E|Z1.$=B' );
define( 'NONCE_SALT',       'w>bXoGP<_KZFbAsRpY`iWl4At6%W81<LhCMN+U1>n~~Z<%5$=^jbv20ts)&x=E1X' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'naturedia_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
