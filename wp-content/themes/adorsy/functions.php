<?php
/**
 * Set up the content width value based on the theme's design.
 *
 * @see tmpmela_content_width()
 *
 * @since TemplateMela 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1400;
}
function tmpmela_setup() {
	/*
	* Makes Templatemela available for translation.
	*
	* Translations can be added to the /languages/ directory.
	* If you're building a theme based on tm, use a find and
	* replace to change 'adorsy' to the name of your theme in all
	* template files.
	*/
	load_theme_textdomain( 'adorsy', get_template_directory() . '/languages' );
	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/font-awesome.css', '/fonts/css/font-awesome.css', tmpmela_fonts_url() ) );
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );
	global $wp_version;
	if ( version_compare( $wp_version, '3.4', '>=' ) ) {
		add_theme_support( 'custom-background' ); 
	}
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		 'primary'   => esc_html__( 'TM Header Navigation', 'adorsy' ),
		 'header-menu'   => esc_html__( 'TM Header Top Links', 'adorsy' ),
	) );
	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 604, 270, true );
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'tmpmela_setup' );
/********************************************************
**************** TEMPLATE MELA CONTENT WIDTH ******************
********************************************************/
function tmpmela_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tmpmela_content_width', 895 );
}
add_action( 'after_setup_theme', 'tmpmela_content_width', 0 );
/**
 * Getter function for Featured Content Plugin.
 *
 * @since TemplateMela 1.0
 *
 * @return array An array of WP_Post objects.
 */
function tmpmela_get_featured_posts() {
	/**
	 * Filter the featured posts to return in TemplateMela.
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'tmpmela_get_featured_posts', array() );
}
/**
 * A helper conditional function that returns a boolean value.
 * @return bool Whether there are featured posts.
 */
function tmpmela_has_featured_posts() {
	return ! is_paged() && (bool) tmpmela_get_featured_posts();
}
/********************************************************
**************** TEMPLATE MELA SIDEBAR ******************
********************************************************/
function tmpmela_widgets_init() {
	register_sidebar( array(
		'name' => esc_html__( 'Main Sidebar', 'adorsy' ),
		'id' => 'sidebar-1',
		'description' => esc_html__( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'adorsy' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	
}
add_action( 'widgets_init', 'tmpmela_widgets_init' );
/********************************************************
**************** TEMPLATE MELA FONT SETTING ******************
********************************************************/
function tmpmela_fonts_url() {
	$fonts_url = '';
	/* Translators: If there are characters in your language that are not
	 * supported by Source Sans Pro, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'adorsy' );
	/* Translators: If there are characters in your language that are not
	 * supported by Bitter, translate this to 'off'. Do not translate into your
	 * own language.
	 */
	$bitter = _x( 'on', 'Bitter font: on or off', 'adorsy' );
	if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {
		$font_families = array();
		if ( 'off' !== $source_sans_pro )
			$font_families[] = 'Source Sans Pro:300,400,600,300italic,400italic,600italic';
		if ( 'off' !== $bitter )
			$font_families[] = 'Bitter:400,600';
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = esc_url( add_query_arg( $query_args, "//fonts.googleapis.com/css" ));
	}
	return $fonts_url;
}
/********************************************************
************ TEMPLATE MELA SCRIPT SETTING ***************
********************************************************/
function tmpmela_scripts_styles() {
	// Add Poppins fonts, used in the main stylesheet.
	wp_enqueue_style( 'tmpmela-fonts', tmpmela_fonts_url(), array(), null );
	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'FontAwesome', get_template_directory_uri() . '/fonts/css/font-awesome.css', array(), '4.7.0' );
	// Loads our main stylesheet.
	wp_enqueue_style( 'tmpmela-style', get_stylesheet_uri(), array(), '1.0' );
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// Loads JavaScript file with functionality specific to Templatemela.
	wp_enqueue_script( 'tmpmela-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2014-02-01', true );
}
add_action( 'wp_enqueue_scripts', 'tmpmela_scripts_styles' );
/********************************************************
************ TEMPLATE MELA IMAGE ATTACHMENT ***************
********************************************************/
if ( ! function_exists( 'tmpmela_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 * @return void
 */
function tmpmela_the_attached_image() {
	/**
	 * Filter the image attachment size to use.
	 *
	 * @since Templatemela 1.0
	 *
	 * @param array $size {
	 *     @type int The attachment height in pixels.
	 *     @type int The attachment width in pixels.
	 * }
	 */
	$attachment_size     = apply_filters( 'tmpmela_attachment_size', array( 1400, 1100 ) );
	$next_attachment_url = wp_get_attachment_url();
	$post                = get_post();
	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );
	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}
		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );
		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}
	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;
/********************************************************
************ TEMPLATE MELA GET URL **********************
********************************************************/
function tmpmela_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );
	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}
/********************************************************
************ TEMPLATE MELA LIST AUTHOR SETTING**************
********************************************************/
if ( ! function_exists( 'tmpmela_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 * @return void
 */
function tmpmela_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );
	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );
		// Move on if user has not published a post (yet).
		if ( ! $post_count ) {
			continue;
		}
	?>
<div class="contributor">
  <div class="contributor-info">
    <div class="contributor-avatar"><?php echo esc_attr(get_avatar( $contributor_id, 132 )); ?></div>
    <div class="contributor-summary">
      <h2 class="contributor-name"><?php echo esc_attr(get_the_author_meta( 'display_name', $contributor_id )); ?></h2>
      <p class="contributor-bio"> <?php echo esc_attr(get_the_author_meta( 'description', $contributor_id )); ?> </p>
      <a class="contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>"> <?php printf( _n( '%d Article', '%d Articles', $post_count, 'adorsy' ), $post_count ); ?> </a> </div>
    <!-- .contributor-summary -->
  </div><!-- .contributor-info -->
</div><!-- .contributor -->
<?php
	endforeach;
}
endif;
/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since TemplateMela 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function tmpmela_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} else {
		$classes[] = 'masthead-fixed';
	}
	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}
	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
	}
	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}
	if ( is_front_page() && 'slider' == get_theme_mod( 'tmpmela_Featured_Content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}
	return $classes;
}
add_filter( 'body_class', 'tmpmela_body_classes' );
/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function tmpmela_post_classes( $classes ) {
	if ( ! post_password_required() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}
	return $classes;
}
add_filter( 'post_class', 'tmpmela_post_classes' );
/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function tmpmela_wp_title( $title, $sep ) {
	global $paged, $page;
	if ( is_feed() ) {
		return $title;
	}
	// Add the site name.
	$title .= get_bloginfo( 'name' );
	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}
	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'adorsy' ), max( $paged, $page ) );
	}
	return $title;
}
add_filter( 'wp_title', 'tmpmela_wp_title', 10, 2 );
// Implement Custom Header features.
get_template_part( 'inc/custom-header' );
// Custom template tags for this theme.
get_template_part( 'inc/template-tags' );
// Add Theme Customizer functionality.
get_template_part( 'inc/customizer' );
/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own tmpmela_Featured_Content class on or
 * before the 'setup_theme' hook.
*/
if ( ! class_exists( 'tmpmela_Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {	
	get_template_part( 'inc/featured-content' );
}
function tmpmela_title_tag() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'tmpmela_title_tag' );
/*Add Templatemela custom function */
get_template_part( 'templatemela/megnor-functions' );
/*Add Templatemela theme setting in menu */
get_template_part( 'templatemela/options' );
/*Add TGMPA library file */
get_template_part( 'templatemela/tmpmela-plugins-install' );
add_action( 'admin_menu', 'tmpmela_theme_setting_menu' );
function tmpmela_theme_settings_page() {
	$locale_file = get_template_part('templatemela/admin/theme-setting');
	if (is_readable( $locale_file ))		
		get_template_part( 'templatemela/admin/theme-setting' );
}
function tmpmela_hook_manage_page() {
	$locale_file = get_template_part('templatemela/admin/theme-hook') ;
	if (is_readable( $locale_file ))		
		get_template_part( 'templatemela/admin/theme-hook' );
}
/* Control Panel Tags Function Includes */
get_template_part( 'templatemela/controlpanel/tmpmela_control_panel' );
get_template_part( 'templatemela/admin/hook-functions' );
get_template_part( 'mr-image-resize' );
/* Adds woocommerce functions if active */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) :
	get_template_part( 'templatemela/woocommerce-functions' );
endif;
/*for single post related page */
if ( ! function_exists( 'tmpmela_related_posts' ) ) :
function tmpmela_related_posts() { /*
global $post;
	$orig_post = $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 12, // Number of related posts that will be shown.
'ignore_sticky_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {   ?>
<div class="related_posts_outer"><div class="shortcode-title center "><h1 class="normal-title"><?php echo esc_attr_e( 'Related Post', 'adorsy' ) ?></h1></div>
<div class="related_posts blog-list">
<?php while( $my_query->have_posts() ) {
$my_query->the_post();?>
<article class="post">
<div class="entry-main-content">
<div class="entry-thumbnail-outer">
<div class="entry-thumbnail">
	<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
</div>
</div>
 <?php $postImage = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );	?>
<div class="post-content <?php if(empty($postImage)): ?> non <?php endif; ?>">
<div class="post-date <?php if(empty($postImage)): ?> non <?php endif; ?>">
		<div class="post-day"> <?php echo get_the_date('d'); ?> </div>
		<div class="post-month"> <?php echo get_the_date('M'); ?> </div>			
</div>
	<div class="post-detail">			
		<div class="post-title">
			<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title_attribute(); ?></a>
		</div>
		<div class="post-description"> <?php echo tmpmela_blog_post_excerpt(80); ?> </div>
	</div>
</div>	
</article>		
<?php
}
echo '</div></div>';
}
}
$post = $orig_post;
wp_reset_postdata(); */
}
endif;
/********************************************************
**************** One Click Import Data ******************
********************************************************/

if ( ! function_exists( 'sampledata_import_files' ) ) :
function sampledata_import_files() {
    return array(
		 array(
            'import_file_name'             => 'adorsy',
            'local_import_file'            => trailingslashit( get_stylesheet_directory() ) . 'demo-content/default/adorsy.wordpress.xml',
            'local_import_customizer_file' => trailingslashit( get_stylesheet_directory() ) . 'demo-content/default/adorsy_customizer_export.dat',
			'local_import_widget_file'     => trailingslashit( get_stylesheet_directory() ) . 'demo-content/default/adorsy_widgets_settings.wie',
            'import_notice'                => esc_html__( 'Please waiting for a few minutes, do not close the window or refresh the page until the data is imported.', 'adorsy' ),
        ),
		);
}
add_filter( 'pt-ocdi/import_files', 'sampledata_import_files' );
endif;

if ( ! function_exists( 'sampledata_after_import' ) ) :
function sampledata_after_import($selected_import) {
         //Set Menu
        $header_menu = get_term_by('name', 'MainMenu', 'nav_menu');
        $top_menu = get_term_by('name', 'Header Top Links', 'nav_menu');
        set_theme_mod( 'nav_menu_locations' , array( 
		 'primary'   => $header_menu->term_id,
		 'header-menu'   => $top_menu->term_id
         ) 
        );
		
		//Set Front page and blog page
       $page = get_page_by_title( 'Home');
       if ( isset( $page->ID ) ) {
        update_option( 'page_on_front', $page->ID );
        update_option( 'show_on_front', 'page' );
       }
	   $post = get_page_by_title( 'Blog');
       if ( isset( $page->ID ) ) {
        update_option( 'page_for_posts', $post->ID );
        update_option( 'show_on_posts', 'post' );
       }
	   
	   //Import Revolution Slider
       if ( class_exists( 'RevSlider' ) ) {
           $slider_array = array(
              get_stylesheet_directory()."/demo-content/default/tmpmela_homeslider.zip", 
              );
           $slider = new RevSlider();
        
           foreach($slider_array as $filepath){
             $slider->importSliderFromPost(true,true,$filepath);  
           }
           echo esc_html__( 'Slider processed', 'adorsy' );
      }
}
add_action( 'pt-ocdi/after_import', 'sampledata_after_import' );
endif;

function tmpmela_change_time_of_single_ajax_call() {
	return 10;
}
add_action( 'pt-ocdi/time_for_one_ajax_call', 'tmpmela_change_time_of_single_ajax_call' );
/* remove notice info*/
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' ); 

function custom_woocommerce_product_add_to_cart_text( $text ) {
	return __( "J'en profite" );
}
add_filter( 'woocommerce_product_add_to_cart_text', 'custom_woocommerce_product_add_to_cart_text' );

/*
* Afficher les articles sur la catégorie des produits
*/
remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_taxonomy_archive_description', 100);
function woocommerce_taxonomy_archive_description() {
	global $wp_query;
	if (is_tax(array( 'product_cat', 'product_tag' ))): ?>
		<div class="term-description">
			<h2><?php echo woocommerce_page_title(); ?></h2>
			<?php echo apply_filters('the_content', term_description()); ?>
		</div><?php
		$currentTerm = $wp_query->get_queried_object();
		$args = [
			'category_name' => $currentTerm->slug,
			'showposts' => 3
		];
		$i = 1;
		wp_reset_postdata();
		
		$output = '';
		$blog_array = new WP_Query( $args );	
		$count = $blog_array->post_count;
		$output = '';
		$items_per_column = 3;
		$type = 'slider';
		if ( $blog_array->have_posts() ):
		$output .= '<div id="blog-posts-products" class="blog-posts-content posts-content '.$type.' conseil-d-expert">';
		$output .= '<h2>CONSEILS D’EXPERT</h2>';
		if($type == "slider") { 
			if($count > $items_per_column)
				$output .= '<div id="'.$items_per_column.'_blog_carousel" class="slider blog-carousel style-'.$style.'">';
			else
				$output .= '<div id="blog_grid" class="blog-grid grid cols-'.$items_per_column.'">';
		} else {
			$output .= '<div id="blog_grid" class="blog-grid grid cols-'.$items_per_column.'">';
		}
		
		while ( $blog_array->have_posts() ) : $blog_array->the_post();
				
			if($i % $items_per_column == 1 )
				$class = " first";
			elseif($i % $items_per_column == 0 )
				$class = " last";
			else
				$class = "";
			$post_day = get_the_date('d');
			$post_month = get_the_date('M');
			$post_year = get_the_date('Y');
			$post_author = get_the_author();
			$args = array(
				   'status' => 'approved',
				   'number' => '5',
				   'post_id' => get_the_ID()
			    );
			 $comments = wp_count_comments(get_the_ID()); 				   
			if ( has_post_thumbnail() && ! post_password_required() ) :	
				$post_thumbnail_id = get_post_thumbnail_id();
				$image = wp_get_attachment_url( $post_thumbnail_id );
			else:
				$image = get_template_directory_uri()."/images/placeholders/placeholder.jpg";					
			endif;
			$src = tmpmela_mr_image_resize($image, $width, $height, true, 't', false);
			if( empty ( $src ) || $src == 'image_not_specified' ):
				$src = get_template_directory_uri()."/images/megnor/placeholder.png";
				$src = tmpmela_mr_image_resize($src, $width, $height, true, 't', false);			
			endif;
			$output .= '<div class="item container '.$class.'  style-'.$style.'">';
			$output .= '<div class="container-inner">';

			$output .= '<div class="post-image">';
			$output .= '<img src="'.$src.'" title="'.get_the_title().'" alt="'.get_the_title().'" />';
			$output .= '<div class="post-title"><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></div>';
			$output .= '</div>';
			$output .= '<div class="post-content-inner">';
			// $output .= '<div class="post-date">';
			// $output .= '<div class="post-day">'.$post_day.'</div>';
			// $output .= '<div class="post-month">'.$post_month.'</div>';
			// $output .= '</div>';
			$output .= '<div class="post-detail">';
			$output .= '<div class="post-description"><p>'.tmpmela_blog_post_excerpt(80).'... <a class="read-more" href="'.get_permalink().'">Lire la suite</a></p></div>';
			$output .= '</div>';
			$output .= '</div>';

			$output .= '</div></div>';
			$i++;
		endwhile;
		$output .= $linktextvariable;
		wp_reset_postdata();
		$output .=	'</div></div>';
		else:
		$output .= '<div class="no-result">'.esc_html__('No results found...', 'adorsy').'</div>';
		endif;
		echo $output;
	endif;
}

/*
* Désactiver la mise à jour sur ce plugin car je l'ai modifié
*/
function disablePluginsUpdates($allPlugins) {
	if (isset($allPlugins) && is_object($allPlugins)):
		if (isset($allPlugins->response['ajax-search-for-woocommerce/ajax-search-for-woocommerce.php'])):
			unset($allPlugins->response['ajax-search-for-woocommerce/ajax-search-for-woocommerce.php']);
		endif;
		if (isset($allPlugins->response['dynamic-price-and-discounts-for-woocommerce/phoen_discounts.php'])):
			unset($allPlugins->response['dynamic-price-and-discounts-for-woocommerce/phoen_discounts.php']);
		endif;
		if (isset($allPlugins->response['giftable-for-woocommerce/giftable-for-woocommerce.php'])):
			unset($allPlugins->response['giftable-for-woocommerce/giftable-for-woocommerce.php']);
		endif;
		if (isset($allPlugins->response['js_composer/js_composer.php'])):
			unset($allPlugins->response['js_composer/js_composer.php']);
		endif;
		if (isset($allPlugins->response['woocommerce-advanced-gift/main.php'])):
			unset($allPlugins->response['woocommerce-advanced-gift/main.php']);
		endif;
	endif;
	return $allPlugins;
}
add_filter( 'site_transient_update_plugins', 'disablePluginsUpdates' );

/*
* Ajouter des champs personnalisés au formulaire d'inscription
*/
function extraRegisterFields() { ?>
	<p class="form-row form-row-wide">
		<label for="reg_billing_gender">
			<?php _e( 'Civilité', 'woocommerce' ); ?><span class="required">*</span>
		</label>
		<input <?php checked( $_POST['billing_gender'], 'Mme' ); ?> checked id="reg_billing_gender" type="radio" name="billing_gender" value="Mme">Mme</label>
		<input <?php checked( $_POST['billing_gender'], 'Mr' ); ?> id="reg_billing_gender" type="radio" name="billing_gender"value="Mr">Mr</label>
	</p>
	<p class="form-row form-row-wide">
		<label for="reg_billing_last_name">
			<?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span>
		</label>
		<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
	</p>
	<p class="form-row form-row-wide">
		<label for="reg_billing_first_name">
			<?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span>
		</label>
		<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
	</p><?php
}
add_action( 'woocommerce_register_form_start', 'extraRegisterFields' );

/**
* Validation des champs ajouté
*/
function validateExtreaRegisterFields( $username, $email, $validation_errors ) {
	extract( $_POST );
	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
		$validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
		$validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
	}

	if ( isset( $_POST['billing_gender'] ) && empty( $_POST['billing_gender'] ) ) {
		$validation_errors->add( 'billing_gender_error', __( '<strong>Error</strong>: Gender is required!.', 'woocommerce' ) );
	}

    if ( strcmp( $password, $password2 ) !== 0 ) {
    	$validation_errors->add( 'password2', __( 'Passwords do not match.', 'woocommerce' ) );
    }

}
add_action( 'woocommerce_register_post', 'validateExtreaRegisterFields', 10, 3 );

/**
* Sauvegarde des données
*/
function saveExtraRegisterFields( $customer_id ) {
	if ( isset( $_POST['billing_first_name'] ) ) {
		update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
		update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
	}

	if ( isset( $_POST['billing_last_name'] ) ) {
		update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
		update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
	}

	if ( isset( $_POST['billing_gender'] ) ) {
		update_user_meta( $customer_id, 'gender', sanitize_text_field( $_POST['billing_gender'] ) );
		update_user_meta( $customer_id, 'billing_gender', sanitize_text_field( $_POST['billing_gender'] ) );
	}
}
add_action( 'woocommerce_created_customer', 'saveExtraRegisterFields' );

function addGenderToEditForm() {
    $user_id = get_current_user_id();
    $current_user = get_userdata( $user_id );
    if (!$current_user) return;
    $billing_gender = get_user_meta( $user_id, 'billing_gender', true ); ?>
    <fieldset>
        <legend>Other information</legend>
        <p class="form-row form-row-first">
	        <label for="reg_billing_gender"><?php _e( 'Civilité', 'woocommerce' ); ?></label>
	        <select class="input-text" name="billing_gender" id="reg_billing_gender">
	            <option <?php if ( esc_attr($billing_gender) == 'Mme') esc_attr_e( 'selected' ); ?> value="Mme">Mme</option>
	            <option <?php if ( esc_attr($billing_gender) == 'Mr') esc_attr_e( 'selected' ); ?> value="Mr">Mr</option>
	        </select>
        </p>
        <div class="clear"></div>
    </fieldset>
    <?php
}

function saveGenderEditForm( $user_id ) {
    update_user_meta($user_id, 'billing_gender', sanitize_text_field($_POST['billing_gender']));
    update_user_meta($user_id, 'gender', sanitize_text_field($_POST['billing_gender']));
}
add_action( 'woocommerce_edit_account_form', 'addGenderToEditForm' );
add_action( 'woocommerce_save_account_details', 'saveGenderEditForm' );

add_action( 'woocommerce_register_form', 'wc_register_form_password_repeat' );
function wc_register_form_password_repeat() {
    ?>
    <div class="form-row form-row-wide addtafterpassword" style=" margin-top: 20px;    margin-bottom: 0;">
        <label for="reg_password2">
        	<?php _e( 'Je confirme mon mot de passe', 'woocommerce' ); ?> <span class="required">*</span>
        </label>
        <input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
    </div><?php
}

/**
 * Filter the cart template path to use our cart.php template instead of the theme's
 */
function csp_locate_template( $template, $template_name, $template_path ) {
	$basename = basename( $template );
	if( $basename == 'form-login.php' ) {
		$template = trailingslashit( dirname( __FILE__ ) ) . 'woocommerce/global/form-login.php';
	} 
	if( $basename == 'cart.php' ) {
		$template = trailingslashit( dirname( __FILE__ ) ) . 'woocommerce/global/cart.php';
	}
	if( $basename == 'up-sells.php' ) {
		$template = trailingslashit( dirname( __FILE__ ) ) . 'woocommerce/single-product/up-sells.php';
	}
	if( $basename == 'cart-totals.php' ) {
		$template = trailingslashit( dirname( __FILE__ ) ) . 'woocommerce/global/cart-totals.php';
	}
	return $template;
}
add_filter( 'woocommerce_locate_template', 'csp_locate_template', 10, 3 );

/*
* Afficher le formulaire
*/ 

add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );

add_action('add_meta_boxes', 'addMetaBox');
add_action('save_post', 'saveDataOfMetaBox');
function addMetaBox() {
    add_meta_box('meta-box', 'Les champs personalisés', 'metaBoxContent', ['product'], 'normal', 'high');
}

function metaBoxContent($post) {
    $setFieldsData = array(
        'description' => ['type' => 'textarea', 'label' => 'Informations complémentaires'],
    );
    wp_nonce_field('metaBoxNonce', 'meta_box_nonce');
    $getPostCustom = get_post_custom($post->ID);
    if($setFieldsData):
        foreach($setFieldsData AS $metaKey => $keyInfos):
            $isExists = isset($getPostCustom[$metaKey][0]) ? esc_attr($getPostCustom[$metaKey][0]) : ''; ?>
            <p><?php if($keyInfos['type'] == 'text'): ?>
                <label for="post_meta[<?php echo $metaKey; ?>]">
                    <?php echo $keyInfos['label']; ?>
                </label><?php 
                endif;
                if($keyInfos['type'] == 'text'): ?>
                    <input type="text" name="post_meta[<?php echo $metaKey; ?>]" id="post_meta[<?php echo $metaKey; ?>]" style="width: 100%" value="<?php echo $isExists;  ?>"/><?php
                else:
                    wp_editor(esc_attr($isExists) ? stripslashes($isExists) : '', $metaKey, [
                        'textarea_name' => "post_meta[{$metaKey}]",
                    ]);
                endif; ?>
            </p><?php
        endforeach;
    endif;
}

function saveDataOfMetaBox($postID) {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    if (!current_user_can('edit_posts')) return;

    if (isset($_POST['post_meta'])):
        if($_POST['post_meta']):
            foreach($_POST['post_meta'] AS $metaKey => $metaValue):
                if(trim($metaValue) != ''):
                    update_post_meta($postID, $metaKey, $metaValue);
                endif;
            endforeach;
        endif;
    endif;
}

add_filter( 'woocommerce_product_tabs', 'woo_customize_tabs', 100, 1 );
function woo_customize_tabs( $tabs ) {
	global $post;
	$getDescription = get_post_meta($post->ID, 'description', true);
    if($getDescription != ''){
       $tabs['reviews']['priority'] = 30;
        $reviews = $tabs['reviews'];
        unset($tabs['reviews']);
        $tabs['additional_information'] = array(
            'title'     => __( 'Additional information', 'woocommerce' ),
            'priority'  => '20',
            'callback'  => 'woocommerce_product_additional_information_tab',
        );
        $tabs['reviews'] = $reviews;
    }
    return $tabs;
}

function woocommerce_product_additional_information_tab($post) {
	global $post;
	$getDescription = get_post_meta($post->ID, 'description', true);
    if($getDescription != ''):
    	echo apply_filters('the_content', $getDescription);
    endif;
}
/*
add_action('woocommerce_after_add_to_cart_form','cmk_additional_button');
function cmk_additional_button() {
	global $product;
	$discountMode = get_post_meta($product->get_id(), 'phoen_woocommerce_discount_mode', true);
	var_dump($discountMode);
	if ( $product->is_in_stock() ) : ?>

		<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
			<?php
			woocommerce_quantity_input( array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			) );

			?>

			<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		</form>


	<?php endif; 
}
*/

function changeProductPriceCart( $price, $cart_item, $cart_item_key ) {
	$product = wc_get_product($cart_item['product_id']);
    if ( !$product->is_type( array( 'variable', 'grouped' ))) {
		$regular_price = $product->get_regular_price();
	    $sale_price = $product->get_sale_price();

	    if($regular_price != $sale_price && $regular_price != 0){
			$discount = round(100-(((float)$sale_price / (float)$regular_price)*100));
	    	$class = floor((((float)$sale_price / (float)$regular_price)*100)/10)*10;
	    }

	    if (isset($discount) && $class!=0 && $regular_price != 0) {
			$price = "<del>".wc_price($regular_price)."</del> <span class='sale-price'>".wc_price($sale_price)."</span> <span class='onsale-table'>-{$discount}%</span>";
	    }
    }
	return $price;
}	
add_filter( 'woocommerce_cart_item_price', 'changeProductPriceCart', 10, 3 );

// Removes Order Notes Title - Additional Information & Notes Field
add_filter( 'woocommerce_enable_order_notes_field', '__return_false', 9999 );
// Remove Order Notes Field
add_filter( 'woocommerce_checkout_fields' , 'remove_order_notes' );
function remove_order_notes( $fields ) {
     unset($fields['order']['order_comments']);
     return $fields;
}

add_action( 'woocommerce_product_query', 'prefix_custom_pre_get_posts_query' );
function prefix_custom_pre_get_posts_query( $q ) {
	if( is_shop() ) { 
	    $tax_query = (array) $q->get( 'tax_query' );
	
	    $tax_query[] = array(
	           'taxonomy' => 'product_cat',
	           'field'    => 'slug',
	           'terms'    => array( 'cadeaux' ), 
	           'operator' => 'NOT IN'
	    );
	
	    $q->set( 'tax_query', $tax_query );
	}
}