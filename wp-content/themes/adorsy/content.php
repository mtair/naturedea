<?php
/**
 * The template for displaying posts in the Standard post format
 *
 * @package WordPress
 * @subpackage TemplateMela
 * @since TemplateMela 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="entry-main-content">
	<?php if ( is_search() || !is_single()) : // Only display Excerpts for Search and not single pages ?>
	  <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<div class="entry-thumbnail">
		 <div class="entry-content-inner">	 
			<?php 
				 the_post_thumbnail('tmpmela-blog-posts-list');
               $postImage = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
				?>
		</div>	
		</div>		
		  <?php else : ?>			
					<?php if ($postImage = tmpmela_get_first_post_images(get_the_ID())):?>		
						 <div class="entry-thumbnail">
						  <div class="entry-content-inner">	 
							<span class="blog-posts-list"><img src="<?php echo esc_url($postImage); ?>"></span>
							<?php 
							if(!empty($postImage)): ?>
							<div class="entry-thumbnail-hover">
								<?php tmpmela_post_entry_date(); ?>
								<?php tmpmela_comments_link(); ?>
							</div>
						<?php endif; ?>
							</div>
							</div>	
					<?php endif; ?>				
  		<?php endif; ?>
		<div class="post-info <?php if(empty($postImage)): ?> non <?php endif; ?>">
				<div class="blog-icon-box">
					<?php if(!empty($postImage)): ?> 
					  <a href="<?php esc_url(the_permalink()); ?>" rel="bookmark">
						<div class="post-inner-top">
							<div class="blog-icon"></div>
						</div>
					 </a>
					  <?php  else: ?>
					  <a href="<?php esc_url(the_permalink()); ?>" rel="bookmark">
						 <div class="post-inner-top post-box">
							<div class="blog-icon"></div>
						</div>	
					  </a>
					<?php endif; ?>	
				</div>
						<?php 
							if( $post->post_title == '' ) : 
								$entry_meta_class = "empty-entry-header";
							else :
								$entry_meta_class = "";
							endif; ?>
						 <?php tmpmela_sticky_post(); ?>	
						<header class="entry-header <?php echo esc_attr($entry_meta_class); ?>">					
								<h1 class="entry-title"> <a href="<?php esc_url(the_permalink()); ?>" rel="bookmark">
								<?php the_title_attribute(); ?>
								</a> </h1>					
						</header><!-- .entry-header -->	
							<div class="entry-meta">
							<?php tmpmela_entry_date(); ?>							  							
							<?php tmpmela_author_link(); ?>
							<?php tmpmela_comments_link(); ?>	
							</div><!-- .entry-meta -->
  	<?php endif; ?>
	<div class="entry-content-other">		
		<?php if ( is_search() || !is_single()) : // Only display Excerpts for Search and not single pages ?>
			<div class="entry-summary">
				<div class="excerpt"><?php echo tmpmela_posts_short_description(); ?></div>
			</div><!-- .entry-summary -->			
		</div><!-- post-info -->
			<?php else : ?>
					<?php 
				if( $post->post_title == '' ) : 
				$entry_meta_class = "empty-entry-header";
				else :
				$entry_meta_class = "";
				endif; ?>
				<header class="entry-header <?php echo esc_attr($entry_meta_class); ?>">					
				<h1 class="entry-title"> 
				<?php the_title_attribute(); ?>
				<?php tmpmela_sticky_post(); ?> </h1>					
				</header><!-- .entry-header -->
						<div class="entry-meta">	 							
							<?php tmpmela_author_link(); ?>
							<?php tmpmela_comments_link(); ?>
							<?php tmpmela_tags_links(); ?>													
							<?php tmpmela_categories_links(); ?>
							<?php edit_post_link( esc_html__( 'Edit', 'adorsy' ), '<span class="edit-link"><i class="fa fa-pencil"></i>', '</span>' ); ?>
							</div><!-- .entry-meta -->
			<div class="entry-content">
			 <?php 
				 the_post_thumbnail('tmpmela-blog-posts-list');
				 $postImage = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
			 ?>
			 <?php the_content( wp_kses( __('Continue reading <span class="meta-nav">&rarr;</span>', 'adorsy' ),tmpmela_allowed_html())); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'adorsy' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
			</div><!-- .entry-content -->	
		<?php endif; ?>
	</div> <!-- entry-content-other -->	
  </div>
</article><!-- #post -->