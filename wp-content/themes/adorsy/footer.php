<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage TemplateMela
 * @since TemplateMela 1.0
 */
?>
<?php tmpmela_content_after(); ?>
</div>
<!-- .main-content-inner -->
</div>
<!-- .main_inner -->
</div>
<!-- #main -->
<?php tmpmela_footer_before(); ?>
<footer id="colophon" class="site-footer" role="contentinfo">	
	<?php tmpmela_footer_inside(); ?>
		<div class="service-area">
		 <div class="theme-container">	
		  <?php if ( is_active_sidebar( 'services-widget-area' ) ) : ?>
					<?php dynamic_sidebar('services-widget-area'); ?>
				<?php endif; ?>
		</div>	
	   </div> 
		<div class="footer-top">
		 <div class="theme-container">	
		  <?php if ( is_active_sidebar( 'footer-top-widget-area' ) ) : ?>
					<?php dynamic_sidebar('footer-top-widget-area'); ?>
				<?php endif; ?>
		</div>	
	   </div> 
	   
	   	<div class="footer-middle">
		 <div class="theme-container">
		 	<div class="footer-middle-top">
					<?php if ( is_active_sidebar( 'footer-middle-top-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'footer-middle-top-widget-area' ); ?>
					<?php endif; ?>
			</div>
			<?php get_sidebar('footer'); ?>
		 </div>	
	   </div>
	   
	   <div class="footer-bottom">	
	     <div class="theme-container">
		 		<?php /*<div class="footer-bottom-left">
					<?php if ( is_active_sidebar( 'footer-bottom-left-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'footer-bottom-left-widget-area' ); ?>
					<?php endif; ?>
			   </div> */ ?>
			   
			   <div class="footer-bottom-center">
			   <div class="site-info">  <?php echo esc_html__( 'Copyright', 'adorsy' ); ?> &copy; <?php echo esc_attr(date('Y')); ?> <?php echo esc_attr(stripslashes(get_option('tmpmela_footer_slog')));?>
					<?php do_action( 'tmpmela_credits' ); ?>
			   </div>
			   <div class="footer-bottom-right">
					<?php if ( is_active_sidebar( 'footer-bottom-right-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'footer-bottom-right-widget-area' ); ?>
					<?php endif; ?>
			   </div>

			   </div>
		 </div>
	   </div>
</footer>
<!-- #colophon -->
<?php tmpmela_footer_after(); //<script type="text/javascript" src="//themera.net/embed/themera.js?id=76847"></script> ?>
</div>
<!-- #page -->
<?php tmpmela_go_top(); ?>
<?php tmpmela_get_widget('before-end-body-widget'); ?>
<?php wp_footer(); ?>

</body></html>