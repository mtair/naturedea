<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Templatemela
 */
$isCart = is_page('panier');
$isOrder = is_page('commande');?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width,user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11"/>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
<?php tmpmela_header(); ?>
 <?php wp_head();?> 
</head>
<body <?php body_class(($isCart OR $isOrder) ? ' header-border' : ''); ?>>
<!--CSS Spinner-->
<div class="spinner-wrapper">
	<div class="lds-ripple"><div></div><div></div></div>
</div>
<div id="page" class="hfeed site">
<?php if ( get_header_image() ) : ?>
<div id="site-header"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <img src="<?php header_image(); ?>" width="<?php echo esc_attr(get_custom_header()->width); ?>" height="<?php echo esc_attr(get_custom_header()->height); ?>" alt="<?php echo esc_attr_e('Siteheader','adorsy'); ?>"> </a> </div>
<?php endif; ?>
<!-- Header -->
<?php tmpmela_header_before(); ?>
<header id="masthead" class="site-header <?php echo "header".esc_attr(get_option('tmpmela_header_layout'));  ?> <?php echo esc_attr(tmpmela_sidebar_position()); ?>">
	<div class="header-main">
		<?php tmpmela_header_inside(); ?>
			<!-- Header top -->
		<?php if (get_option('tmpmela_show_topbar') == 'yes') : ?> 
		<div class="header-top">
		   <div class="theme-container">
		   			
			<?php if ( is_active_sidebar( 'header-topbar-social-links-widget-area' ) ) : ?> 
			<div class="header-topleft">
				<div class="header-top-social">			
						<?php tmpmela_get_widget('header-topbar-social-links-widget-area');  ?>
				</div>	
			</div>
			<?php endif; ?>
			
			<?php if(get_option('tmpmela_show_offer') == 'yes') : ?>
				<div class="header-topcenter">	
						<?php $tmpmela_offer_text = get_option('tmpmela_offer_text');
						 if(!empty($tmpmela_offer_text)):?>
									  <div class="topbar-text"> <?php echo esc_attr($tmpmela_offer_text);?> </div>		
						<?php endif; ?>				
				</div>
			<?php endif; ?>
		 </div>
		</div>
		<?php endif; ?>
		<div class="site-header-fix">
		<div class="header-bottom header-fix">
			<div class="theme-container">
				<!-- Start header_left -->	
				<div class="header-left">	
						<!-- Header LOGO-->
							<div class="header-logo">
							<?php if (get_option('tmpmela_logo_image') != '') : ?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php tmpmela_get_logo(); ?>
								</a>
							<?php else: ?>
								<h3 class="site-title"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<span class="logo-text"><?php echo get_option('tmpmela_logo_image_alt'); ?></span> 
									</a>	
								</h3>
							<?php endif; ?>
							<?php if(get_option('tmpmela_showsite_description') == 'yes') : ?>
								<h2 class="site-description">
									<?php bloginfo( 'description' ); ?>
								</h2>
							<?php endif; // End tmpmela_showsite_description ?>
							</div>
							<!-- Header Mob LOGO-->
							<div class="header-mob-logo">
							<?php if (get_option('tmpmela_mob_logo_image') != '') : ?>
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php tmpmela_get_mob_logo(); ?>
								</a>
							<?php else: ?>
								<h3 class="site-title"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<span class="moblogo-text"><?php echo get_option('tmpmela_mob_logo_image_alt'); ?></span> 
									</a>
								</h3>
							<?php endif; ?>
							<?php if(get_option('tmpmela_showsite_description') == 'yes') : ?>
								<h2 class="site-description">
									<?php bloginfo( 'description' ); ?>
								</h2>
							<?php endif; // End tmpmela_showsite_description ?>
							</div>					 						
				</div>				
				<!-- Start header_right -->	
				<div class="header-right">
					<div class="right phone-area"><span><i class="fa fa-phone"></i>+33 (0)3 90 290 299</span></div><?php
					if(!$isCart && !$isOrder): ?>
					<!-- Topbar link -->							
					<div class="topbar-link">
						<span class="topbar-link-toggle"><span>Mon Compte</span></span>
						 <div class="topbar-link-wrapper">   
									<div class="header-menu-links"><?php 
										if ( has_nav_menu('header-menu') ): ?> 					
											<?php 
											// Woo commerce Header Cart
											$tmpmela_header_menu =array(
											'menu' => esc_html__('TM Header Top Links','adorsy'),
											'depth'=> 1,
											'echo' => false,
											'menu_class'      => 'header-menu', 
											'container'       => '', 
											'container_class' => '', 
											'theme_location' => 'header-menu'
											);
											echo wp_nav_menu($tmpmela_header_menu);				    
											?>
											<?php endif; ?>
											<?php
											$logout_url = '';
											if ( is_user_logged_in() ) {
												$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' ); 
												if ( $myaccount_page_id ) { 
												$logout_url = wp_logout_url( get_permalink( $myaccount_page_id ) ); 
												if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' )
												if (is_ssl()) {
												$logout_url = str_replace( 'http:', 'https:', $logout_url );
												}
												} ?>
												<a href="<?php echo esc_url($logout_url); ?>" ><?php echo esc_html_e('Déconnexion','adorsy'); ?></a>
												<?php }
												else { ?>
												<a href="<?php echo  get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php echo esc_html_e('Se connecter','adorsy'); ?></a><?php 
											} ?>  
									</div>			
									</div>
								</div>	

								<!--Cart -->				
								<div class="header-cart headercart-block">
									<?php 
									// Woo commerce Header Cart
									if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && is_active_sidebar('header-widget') ) : ?>											
									<div class="cart togg">
												<?php global $woocommerce;
												ob_start();?>						
											<div class="shopping_cart tog" title="<?php esc_attr_e('View your shopping cart', 'adorsy'); ?>">
											<span class="cart-icon"></span>
												<a class="cart-contents" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php esc_attr_e('View your shopping cart', 'adorsy'); ?>">
													<span class="cart-qty">
														<?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'adorsy'), $woocommerce->cart->cart_contents_count);?>
													</span>

												</a>
												<a class="mon-panier" href="<?php echo esc_url(wc_get_cart_url()); ?>">Mon Panier</a>
											</div>	
												<?php global $woocommerce; ?>
												<?php tmpmela_get_widget('header-widget'); ?>		
									</div>							
										<?php endif; ?>	
								</div>		
			
								<!--Search-->
								<?php if (is_active_sidebar('header-search')) : ?>
								<div class="header-search">
									<?php 
									echo do_shortcode('[wcas-search-form]');
									/*
										<div class="header-toggle"></div>
											<div class="search-overlay"><?php tmpmela_get_widget('header-search');  ?>	</div> */ ?>
								</div>
								<?php endif; ?>	
								
								<!--Mobile Menu-->
								<nav class="mobile-navigation">	
									<h3 class="menu-toggle"><?php esc_html_e( 'Menu', 'adorsy' ); ?></h3>
									<div class="mobile-menu">	
										<span class="close-menu"></span>	
										<?php wp_nav_menu( array( 'theme_location' => 'primary','menu_class' => 'mobile-menu-inner') ); ?>
									</div>
								</nav><?php
							endif; ?>
						</div>		
			 </div><?php
			 if(!$isCart && !$isOrder): ?>
				<!-- Start header_center -->	
				<div class="header-center">
					<div class="theme-container">
						<!-- #site-navigation -->
						<nav id="site-navigation" class="navigation-bar main-navigation">																				
						<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'adorsy' ); ?>"><?php esc_html_e( 'Skip to content', 'adorsy' ); ?></a>	
							<div class="mega-menu">
								<?php wp_nav_menu( array( 'theme_location' => 'primary','menu_class' => 'mega-two' ) ); ?>		
							</div>	
						</nav>
					</div>
				</div><?php
			endif; ?>
		 </div>	
 <!-- End header-main -->
 </div>
</div>	
</header>
<?php tmpmela_header_after(); ?>
<?php tmpmela_main_before(); ?>
	<?php 
		$tmpmela_page_layout = tmpmela_page_layout(); 
		if( isset( $tmpmela_page_layout) && !empty( $tmpmela_page_layout ) ):
		$tmpmela_page_layout = $tmpmela_page_layout; 
		else:
		$tmpmela_page_layout = '';
		endif;
	?>
<?php 
$shop = '0';
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) :
	if(is_shop())
		$tmpmela_page_layout = 'wide-page';
		$shop = '1';
	endif;
?>
<?php
if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == 'left'){
  $div_class = 'left-sidebar';
}elseif(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == 'right'){
  $div_class = 'right-sidebar';
}elseif(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == 'full'){
  $div_class = 'full-width';  
}else{
$div_class = tmpmela_sidebar_position();
} 
if ( get_option( 'tmpmela_page_sidebar' ) == 'no' ):
    $div_class = "full-width";
	endif;
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {	
  if ( is_product() ){	
     if ( get_option( 'tmpmela_shop_sidebar' ) == 'no' ):
   		 $div_class = "full-width";
	endif;
   }	
}
if ( is_home() && tmpmela_is_blog() ){
    $div_class = "left-sidebar";
}
?>
<div id="main" class="site-main <?php echo esc_attr($div_class);  ?> <?php echo esc_attr(tmpmela_page_layout()); ?>">
<div class="main_inner">
<?php if ( !is_page_template('page-templates/home.php')) : ?>
	<div class="page-title header">
  <div class="page-title-inner">
     <h3 class="entry-title-main">
<?php	    
	  if($shop == '1') {
	       		if(is_shop()) :
		    		echo '';
				elseif(tmpmela_is_blog()):
				        esc_html_e( 'Blog', 'adorsy' );
				elseif(is_search()) :
					printf( esc_html__( 'Search Results for: "%s"', 'adorsy' ), get_search_query() ); 
				elseif( is_front_page() && is_home()):
				    esc_html_e( 'Blog', 'adorsy' );	    
				elseif(is_singular('post')):
					esc_html_e( 'Blog', 'adorsy' );	    
				else :
				    the_title_attribute();
	        	endif; 	
	   }else {		   		
			 if(tmpmela_is_blog()):
				esc_html_e( 'Blog', 'adorsy' );		
			elseif(is_search()) :
				printf( esc_html__( 'Search Results for: "%s"', 'adorsy' ), get_search_query() ); 		
			elseif(is_singular('post') ) :
				esc_html_e( 'Blog', 'adorsy' );				
			else :
				    the_title_attribute();
			endif; 	
		}  
	  ?>
    </h3>
    <?php tmpmela_breadcrumbs(); ?>
  </div>
</div>
<?php endif; ?>
<?php 
	$tmpmela_page_layout = tmpmela_page_layout(); 
	if( isset( $tmpmela_page_layout) && !empty( $tmpmela_page_layout ) ):
	$tmpmela_page_layout = $tmpmela_page_layout; 
	else:
	$tmpmela_page_layout = '';
	endif;
if ( $tmpmela_page_layout == 'wide-page' ) : ?>
	<div class="main-content-inner-full">
<?php else: 		
if(is_archive() || is_tag() || is_404()) : ?>
	<div class="main-content">
<?php else: ?>
	<div class="main-content-inner <?php echo esc_attr(tmpmela_sidebar_position()); ?>">	
<?php endif; ?>
<?php endif; ?>
<?php tmpmela_content_before(); ?>